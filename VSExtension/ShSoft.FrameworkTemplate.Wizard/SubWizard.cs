﻿using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using System.Collections.Generic;
using System.Text;

namespace ShSoft.FrameworkTemplate.Wizard
{
    public class SubWizard : IWizard
    {
        public static readonly StringBuilder StringBuilder = new StringBuilder();

        /// <summary>
        /// 在模板向导运行的开头运行自定义向导逻辑。
        /// </summary>
        /// <param name="automationObject">模板向导正在使用的自动化对象。</param>
        /// <param name="replacementsDictionary">要替换的标准参数的列表。</param>
        /// <param name="runKind">指示向导运行的类型。</param>
        /// <param name="customParams">在项目中用来执行参数替换的自定义参数。</param>
        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            replacementsDictionary["$safeprojectname$"] = MainWizard.GlobalParameters["$safeprojectname$"];
        }

        /// <summary>
        /// 当项目已完成生成时运行自定义向导逻辑。
        /// </summary>
        /// <param name="project">已完成生成的项目。</param>
        public void ProjectFinishedGenerating(Project project)
        {

        }

        /// <summary>
        /// 当项目项已完成生成时运行自定义向导逻辑。
        /// </summary>
        /// <param name="projectItem">已完成生成的项目项。</param>
        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {

        }

        /// <summary>
        /// 指示是否应将指定的项目项添加到项目中。
        /// </summary>
        /// <param name="filePath">项目项的路径。</param>
        /// <returns>如果应将项目项添加到项目中，则为 true；否则为 false。</returns>
        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        /// <summary>
        /// 在打开模板中的项之前运行自定义向导逻辑。
        /// </summary>
        /// <param name="projectItem">将要打开的项目项。</param>
        public void BeforeOpeningFile(ProjectItem projectItem)
        {

        }

        /// <summary>
        /// 当向导已完成所有任务时运行自定义向导逻辑。
        /// </summary>
        public void RunFinished()
        {

        }
    }
}
