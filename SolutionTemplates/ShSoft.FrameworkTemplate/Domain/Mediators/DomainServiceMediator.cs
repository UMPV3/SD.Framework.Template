﻿using $safeprojectname$.Domain.IDomainServices;

namespace $safeprojectname$.Mediators
{
    /// <summary>
    /// 领域服务中介者
    /// </summary>
    public sealed class DomainServiceMediator
    {
        /// <summary>
        /// 依赖注入构造器
        /// </summary>
        /// <param name="numberSvc">编号领域服务接口</param>
        public DomainServiceMediator(INumberService numberSvc)
        {
            this.NumberSvc = numberSvc;
        }

        /// <summary>
        /// 编号领域服务接口
        /// </summary>
        public INumberService NumberSvc { get; private set; }
    }
}
