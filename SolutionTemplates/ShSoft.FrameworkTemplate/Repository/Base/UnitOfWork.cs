﻿using ShSoft.Infrastructure.Repository.EntityFramework;
using $safeprojectname$.Domain.IRepositories;

namespace $safeprojectname$.Repository.Base
{
    /// <summary>
    /// 工作单元 - 请自行改名
    /// </summary>
    public sealed class UnitOfWork : EFUnitOfWorkProvider, IUnitOfWorkConcrete
    {

    }
}
