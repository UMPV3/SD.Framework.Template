using SD.IOC.Core.Mediator;
using ShSoft.Infrastructure.RepositoryBase;
using System.Data.Entity.Migrations;
using $safeprojectname$.Repository.Base;

namespace $safeprojectname$.Repository.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DbSession>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DbSession context)
        {
            //��ʼ������
            IDataInitializer initializer = ResolveMediator.Resolve<IDataInitializer>();
            initializer.Initialize();
        }
    }
}
