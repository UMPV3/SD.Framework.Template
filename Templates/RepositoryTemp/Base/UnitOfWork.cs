﻿using DomainTemp.IRepositories;
using ShSoft.Infrastructure.Repository.EntityFramework;

namespace RepositoryTemp.Base
{
    /// <summary>
    /// 单元事务 - 请自行改名
    /// </summary>
    public sealed class UnitOfWork : EFUnitOfWorkProvider, IUnitOfWorkConcrete
    {

    }
}
