﻿using ShSoft.Infrastructure.RepositoryBase;

namespace RepositoryTemp.Base
{
    /// <summary>
    /// 数据初始化器实现
    /// </summary>
    public class DataInitializer : IDataInitializer
    {
        /// <summary>
        /// 初始化基础数据
        /// </summary>
        public void Initialize()
        {

        }
    }
}
