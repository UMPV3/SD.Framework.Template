﻿using ShSoft.Infrastructure.RepositoryBase;

namespace DomainTemp.IRepositories
{
    /// <summary>
    /// 工作单元 - 请自行重命名
    /// </summary>
    public interface IUnitOfWorkConcrete : IUnitOfWork
    {

    }
}
