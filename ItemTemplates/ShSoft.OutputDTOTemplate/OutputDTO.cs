﻿using System.Runtime.Serialization;
using ShSoft.Infrastructure.DTOBase;

namespace $rootnamespace$
{
    /// <summary>
    /// XXX数据传输对象
    /// </summary>
    /// <remarks>勿忘[DataMember]</remarks>
    [DataContract(Namespace = "http://$rootnamespace$")]
 	public class $safeitemrootname$ : BaseDTO
	{

	}
}
