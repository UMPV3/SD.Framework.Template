﻿using System;
using System.ServiceModel;
using ShSoft.Infrastructure;
using ShSoft.Infrastructure.DTOBase;

namespace $rootnamespace$
{
    /// <summary>
    /// XXX服务契约接口
    /// </summary>
    [ServiceContract(Namespace = "http://$rootnamespace$")]
	public interface $safeitemrootname$ : IApplicationService
	{
        //命令部分

        #region # 示例 —— void DoCommand()
        /// <summary>
        /// 示例
        /// </summary>
        [OperationContract]
        void DoCommand();
        #endregion



        //查询部分

        #region # 示例 —— string DoQuery()
        /// <summary>
        /// 示例
        /// </summary>
        [OperationContract]
        string DoQuery();
        #endregion
	}
}
