﻿using System.Data.Entity.ModelConfiguration;

namespace $rootnamespace$
{
    /// <summary>
    /// XXX实体映射配置
    /// </summary>
	public class $safeitemrootname$ : EntityTypeConfiguration<>
	{
        /// <summary>
        /// 构造器
        /// </summary>
        public $safeitemrootname$()
        {
            //在此处写Fluent API表达式
        }
	}
}
