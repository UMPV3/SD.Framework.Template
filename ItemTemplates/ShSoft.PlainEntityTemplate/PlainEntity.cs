﻿using System;
using ShSoft.Infrastructure.EntityBase;

namespace $rootnamespace$
{
    /// <summary>
    /// 
    /// </summary>
	public class $safeitemrootname$ : PlainEntity
	{
        #region # 构造器

        #region 01.无参构造器
        /// <summary>
        /// 无参构造器
        /// </summary>
        protected $safeitemrootname$() { }
        #endregion


        #endregion

        #region # 属性



        #endregion

        #region # 方法




        #endregion
	}
}
