﻿using System;

namespace $rootnamespace$
{
    /// <summary>
    /// XXX领域服务实现
    /// </summary>
	public class $safeitemrootname$ : I$safeitemrootname$
	{
        #region # 字段及依赖注入构造器

        /// <summary>
        /// 仓储中介者
        /// </summary>
        private readonly RepositoryMediator _repMediator;

        /// <summary>
        /// 依赖注入构造器
        /// </summary>
        /// <param name="repMediator">仓储中介者</param>
        /// <param name="unitOfWork">单元事务</param>
        public $safeitemrootname$(RepositoryMediator repMediator)
        {
            this._repMediator = repMediator;
        }

        #endregion

        #region # 获取聚合根实体关键字 —— string GetKeywords($safeitemrootname$ entity)
		/// <summary>
        /// 获取聚合根实体关键字
        /// </summary>
        /// <param name="entity">聚合根实体对象</param>
        /// <returns>关键字</returns>
        public string GetKeywords($safeitemrootname$ entity)
        {
            throw new NotImplementedException();
        } 
	#endregion
	}
}
