﻿using System;
using System.ServiceModel;

namespace $rootnamespace$
{
    /// <summary>
    /// XXX服务契约实现
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
	public class $safeitemrootname$ : I$safeitemrootname$
	{
        #region # 字段及依赖注入构造器

        /// <summary>
        /// 领域服务中介者
        /// </summary>
        private readonly DomainServiceMediator _svcMediator;

        /// <summary>
        /// 仓储中介者
        /// </summary>
        private readonly RepositoryMediator _repMediator;

        /// <summary>
        /// 单元事务
        /// </summary>
        private readonly IUnitOfWorkConcrete _unitOfWork;

        /// <summary>
        /// 依赖注入构造器
        /// </summary>
        /// <param name="svcMediator">领域服务中介者</param>
        /// <param name="repMediator">仓储中介者</param>
        /// <param name="unitOfWork">单元事务</param>
        public $safeitemrootname$(DomainServiceMediator svcMediator, RepositoryMediator repMediator, IUnitOfWorkConcrete unitOfWork)
        {
            this._svcMediator = svcMediator;
            this._repMediator = repMediator;
            this._unitOfWork = unitOfWork;
        }

        #endregion

        
        //命令部分

        #region # 示例 —— void DoCommand()
        /// <summary>
        /// 示例
        /// </summary>
        public void DoCommand()
	    {
	        Console.WriteLine("Hello World");
	    }
        #endregion



        //查询部分

        #region # 示例 —— string DoQuery()
        /// <summary>
        /// 示例
        /// </summary>
        public string DoQuery()
	    {
	        return "Hello World";
	    }
        #endregion
	}
}
